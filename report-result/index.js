(function () {
    'use strict';
    var express = require('express');
    var app = express();
    var Rabbit = require('node-rabbitmq');
    var logSender = require('node-log-sender');
    var config = require('ptl-config');
    var redis = require('redis'),
        client = redis.createClient();

    var rabbit = new Rabbit(config.queueUri, function () {
        logSender.configure(rabbit);
        app.get('/', function (req, res) {
            logSender.sendKPI('betReportRequested');
            client.get('betReport', function (err, reply) {
                var report = JSON.parse(reply);

                logSender.sendKPI('betReportSent');

                res.send(report);
            });
        });

        var server = app.listen(3001, function () {
            logSender.info('Report Server Stated http://' + server.address().address + ':' + server.address().port);
        });
    });


    function bail(err) {
        console.error('uncaughtException:', err.message);
        console.error(err.stack);
        logSender.error(err.message)
        logSender.error(err.stack);
    }

    client.on('error', function (err) {
        logSender.error('redis bail');
        bail(err);
    });

    process.on('uncaughtException', function (err) {
        bail(err);
        process.exit(1);
    });
})();
