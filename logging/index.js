(function () {
    "use strict";

    var Rabbit = require("node-rabbitmq");
    var config = require("ptl-config");
    var winston = require('winston');
    require('winston-mongodb').MongoDB;

    var logger = new (winston.Logger)({
        transports: [
            new (winston.transports.Console)({
                colorize: 'all',
                timestamp: true
            }),
            new (winston.transports.DailyRotateFile)({filename: "../logs/logs.log"}),
            new (winston.transports.MongoDB)({db: config.mongoUri})
        ],
        exceptionHandlers: [
            new (winston.transports.Console)({
                colorize: 'all'
            }),
            new winston.transports.File({filename: "../logs/exceptions.log"}),
            new (winston.transports.MongoDB)({db: config.mongoUri, collection: 'exceptions'})
        ]
    });

    var rabbit = new Rabbit(config.queueUri, function () {

        rabbit.receiveJson("logging", function (message, that) {
            saveLog(message);
            that.ack();
        });
    });

    function saveLog(message) {
        switch (message.logType) {
            case "Info":
                logger.info(message.logMessage, {date: message.logDate});
                break;
            case "Warn":
                logger.warn(message.logMessage, {date: message.logDate});
                break;
            case "Error":
                logger.error(message.logMessage, {date: message.logDate});
                break;
            default:
                logger.info(message);

        }
    }

    process.on('uncaughtException', function (err) {
        console.error('uncaughtException:', err.message);
        console.error(err.stack);
        saveLog({logType:  "Error", logMessage:  err.message + " \n" + err.stack});
        process.exit(1);
    });
})();
