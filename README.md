#Pick The Ladder Service for aggregating the Top 10 Selections


There are 5 Services that help complete the task of recording all the Pick The Ladder bets and return the Top 10 to the User

## Prerequisites

 1. Mongodb
 2. Redis
 3. RabbitMq


## Initial Setup

    npm install mocha nodemon grunt-cli -g

  Each service is setup and run the same. Go to the folder for the service and:

    npm install

    # To Run

         nodemon

    # To Test

        grunt spec
## Utility Tools

### PTL-Config

This is a module for sharing config values between the projects

###Logging

This will any log messages passed to it and persist them to the database and display them in the console.

##### Config

 1. RabbitMq Endpoint
 2. MongoDb Endpoint


### KPI Tool

This records all the KPIs and has an endpoint that returns the current count.

#### Config

1. Redis Endpoint

## Services

### Bet Receiver

#### Summary

This is a web endpoint which receives the selections of the bet and adds them to the Queue.

##### Config

This needs to know the Rabbit Endpoint and the port it will listen too for incoming HTTP POSTs

##### KPIs

- Bets Received

### Bet Report

##### Summary

This module has two purposes.

 1. Receiving bets off the Queue and storing them to the Document database
 2. Regenerating the Report which aggregates all the bets and returns the top team for each position.

##### Config
  1. Rabbit Mq Endpoint
  2. MongoDb Endpoint
  3. Redis Endpoint

#### KPIs

 - Bets Saved
 - Reports Updated

### Report Results

#### Summary

This return the latest report to the user.

#### Config

1. Redis Endpoint

#### KPIs

 - Reports Sent
