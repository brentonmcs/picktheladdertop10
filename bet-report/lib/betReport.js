'use strict';

var redis = require('redis'),
    client = redis.createClient();

var BetAg = require('./betAggregation');
var betAg;

var topX = 10;
var logSender, rabbit;

client.on('error', function(err) {
    logSender.error(err);
});

function BetReport(log, config, rabbitMq) {
    logSender = log;
    rabbit = rabbitMq;
    betAg = new BetAg(logSender, config);
}

BetReport.prototype.generateReport = function(callback) {

    var report = {
        total: 0,
        bets: []
    };

    betAg.totalBets(function(err, count) {
        report.total = count;


        for (var i = 0; i < topX; i++) {
            aggregateSelection(i, report, callback);
        }
    });
};


function aggregateSelection(i, report, callback) {
    betAg.aggregateSelection(i, function(result, pos) {

        report.bets.push({
            selection: pos,
            competitor: result._id,
            bets: result.total
        });
        checkEnd(report, callback);
    });
}

function checkEnd(report, callback) {

    if (report.bets.length === 10) {

        client.set('betReport', JSON.stringify(report));
        logSender.sendKPI('ReportUpdated');
        callback();
    }
}

module.exports = BetReport;
