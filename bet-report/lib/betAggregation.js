'use strict';

var util = require('util');
var MongoEventer = require('node-mongoEventer');

var betAg;
var logSender;

var totalBetsEventName = 'totalBets';
var aggregateResultEventName = 'aggregateResult';


function BetAggregation(logSend, configValues) {
    betAg = this;
    logSender = logSend;

    MongoEventer.call(this, logSender, configValues);

    betAg.on(aggregateResultEventName, aggregateResult);
    betAg.on(totalBetsEventName, function (eventObject) {
        eventObject.collection.count(function(err, count) {
            eventObject.callback(err, count);
            eventObject.db.close();
        });
    });
}

util.inherits(BetAggregation, MongoEventer);

BetAggregation.prototype.totalBets = function(callback) {

    betAg.emit('connectToDatabase', {
        callback: callback,
        returnEvent: totalBetsEventName,
        collectionName: 'bets',
        parentObject : betAg
    });
};

BetAggregation.prototype.aggregateSelection = function(position, callback) {
    betAg.emit('connectToDatabase', {
        position: position,
        callback: callback,
        returnEvent: aggregateResultEventName,
        collectionName: 'bets',
        parentObject : betAg
    });
};

function aggregateResult(eventObject) {
    eventObject.collection.aggregate([{
        $group: {
            _id: '$Selection' + eventObject.position,
            total: {
                $sum: 1
            }
        }
    }, {
        $sort: {
            total: -1
        }
    }, {
        '$limit': 1
    }], function(err, docs) {
        if (err) {
            logSender.error(err);
            return;
        }
        eventObject.callback(docs[0], eventObject.position);
        eventObject.db.close();
    });
}

module.exports = BetAggregation;
