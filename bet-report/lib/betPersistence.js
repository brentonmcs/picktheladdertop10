'use strict';

var util = require('util');
var MongoEventer = require('node-mongoEventer');
var betPer, logSender;

function BetPersistence(logSend, configValues) {
    betPer = this;
    logSender = logSend;
    MongoEventer.call(this, logSender, configValues);
    betPer.on('insertBet', insertRecord);
}

util.inherits(BetPersistence, MongoEventer);

BetPersistence.prototype.insertBet = function (message, callback) {

    if (message.competitors && message.competitors.competitors) {
        betPer.emit('connectToDatabase', {
            callback: callback,
            returnEvent: 'insertBet',
            collectionName: 'bets',
            competitors: message.competitors.competitors
        });
    }
};

function insertRecord(eventObject) {

    var competitors = eventObject.competitors;
    var record = {
        selection1: competitors[0],
        selection2: competitors[1],
        selection3: competitors[2],
        selection4: competitors[3],
        selection5: competitors[4],
        selection6: competitors[5],
        selection7: competitors[6],
        selection8: competitors[7],
        selection9: competitors[8],
        selection10: competitors[9],
        selection11: competitors[10],
        selection12: competitors[11],
        selection13: competitors[12],
        selection14: competitors[13],
        selection15: competitors[14],
        selection16: competitors[15],
        selection17: competitors[16],
        selection18: competitors[17],
        added: new Date()
    };

    eventObject.collection.insert(record, function (err) {
        if (err) {
            logSender.error(err);
            return console.log(err);
        }

        eventObject.callback();
    });
}

module.exports = BetPersistence;
