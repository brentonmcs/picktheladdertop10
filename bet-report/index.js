(function () {
    'use strict';
    var Rabbit = require('node-rabbitmq');
    var config = require('ptl-config');
    var BetReport = require('./lib/betReport');
    var logSender = require("node-log-sender");
    var BetPersistence = require('./lib/betPersistence');

    var rabbit = new Rabbit(config.queueUri, function () {

        logSender.configure(rabbit);
        var betReport = new BetReport(logSender, config, rabbit);

        rabbit.receiveJson('betSaved', function (message, that) {
            betReport.generateReport(function () {
                that.ack();
            });
        });

        var betPersistence = new BetPersistence(logSender, config);

        rabbit.receiveJson('bet', function (message, that) {
            betPersistence.insertBet(message, function () {
                rabbit.sendJson({}, 'betSaved');
                logSender.sendKPI('Bet saved');
                that.ack();
            });
        });
    });

    process.on('uncaughtException', function (err) {
        console.error('uncaughtException:', err.message);
        console.error(err.stack);
        logSender.error(err.message + " \n" + err.stack);
        process.exit(1);
    });
})();
