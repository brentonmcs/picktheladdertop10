'use strict';

require('../index');

var assert = require('chai').assert;
var config = require('ptl-config');
var Rabbit = require('node-rabbitmq');
var rabbit, rabbitKpi;


describe('when a bet is saved the report should be run', function () {

    beforeEach(function (done) {
        rabbit = new Rabbit(config.queueUri, function () {
            rabbitKpi = new Rabbit(config.queueUri, function () {
                done();
            });
        });
    });

    it('should send a KPI to queue when done', function (done) {

        //rabbit.receiveJson('logging', function (message, that) {
        //    that.ack();
        //});
        rabbit.receiveJson('KPI', function (message, that) {
            that.ack();
            //assert.equal('ReportUpdated', message.kpiName);
            done();
        });

        rabbit.sendJson({

            competitors: {
                competitors: [
                    99, 98, 97, 96, 95, 94, 93, 92, 91, 90
                ]
            }
        }, 'bet');

        setTimeout(function () {
            done('queue not fired');
        }, 1000);

    });
})
;
