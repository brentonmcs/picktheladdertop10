'use strict';

var assert = require('chai').assert;
var config = require('../../config');
var MongoClient = require('mongodb').MongoClient;
var BetPersistence = require('../lib/betPersistence');
var Rabbit = require('node-rabbitmq');
var logSender = require('node-log-sender');

var rabbit;

var betPersistence;

function clearDb(callback) {
    MongoClient.connect(config.mongoUri, function(err, db) {
        db.collection('bets', function(err, col) {

            col.remove({}, function(err) {
                assert.equal(null, err);
                db.close();
                callback();
            });

        });
    });
}

function findRecord(selection1, selection2, selection3, callback) {
    MongoClient.connect(config.mongoUri, function(err, db) {
        db.collection('bets', function(err, col) {
            assert.equal(null, err, err);
            col.findOne({
                selection1: selection1,
                selection2: selection2,
                selection3: selection3
            }, function(err, rec) {
                assert.equal(null, err);
                assert.isNotNull(rec);
                callback();
                db.close();
            });

        });
    });
}

describe('when the bet is inserted', function() {

    beforeEach(function(done) {

        rabbit = new Rabbit(config.queueUri, function () {
            logSender.configure(rabbit);
            betPersistence = new BetPersistence(logSender, config, rabbit);
            clearDb(function() {
                done();
            });
        });

    });

    var message = {
        competitors: {
            competitors: [
                99, 98, 97, 96, 95, 94, 93, 92, 91, 90
            ]
        }
    };
    //it('should be saved to the database', function(done) {
    //    betPersistence.insertBet(message, function() {
    //        findRecord(99, 98, 97, function() {
    //            done();
    //
    //        });
    //    });
    //});


});
