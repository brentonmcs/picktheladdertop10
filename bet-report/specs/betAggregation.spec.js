'use strict';

var assert = require('chai').assert;
var MongoClient = require('mongodb').MongoClient;
var config = require('../../config');
var BetAg = require('../lib/betAggregation');
var Rabbit = require('node-rabbitmq');
var logSender = require('node-log-sender');

var rabbit;


var testBets = [{
    Selection1: 1,
    Selection2: 7,
    Selection3: 7
}, {
    Selection1: 1,
    Selection2: 7,
    Selection3: 7
}, {
    Selection1: 1,
    Selection2: 7,
    Selection3: 7
}, {
    Selection1: 2,
    Selection2: 7,
    Selection3: 7
}, {
    Selection1: 3,
    Selection2: 7,
    Selection3: 7
}];

function clearDb(done) {
    MongoClient.connect(config.mongoUri, function(err, db) {
        db.collection('bets', function(err, col) {

            col.remove({}, function(err, res) {
                assert.equal(null, err);
                col.insert(testBets,
                    function(err, rec) {
                        assert.equal(null, err);

                        done();
                    });
            });

        });
    });
}

describe('counting the bets for percentages', function() {

    var betAg = new BetAg(logSender, config);

    before(function(done) {

        rabbit = new Rabbit(config.queueUri, function () {
            logSender.configure(rabbit);

            clearDb(done);
        });

    });

    //it('should return totalBets', function(done) {
    //    betAg.totalBets(function(err, count) {
    //        assert.equal(5, count);
    //        done();
    //    });
    //});
});

describe('aggregate each selection', function() {

    var betAg = new BetAg(logSender, config);

    before(function(done) {
        clearDb(done);
    });

    it('should return competitor 1 for selection 1', function(done) {
        betAg.aggregateSelection(1, function(result) {
            assert.equal(1, result._id);
            done();
        });
    });

    it('should return 3 for selection 1 total', function(done) {
        betAg.aggregateSelection(1, function(result) {
            assert.equal(3, result.total);
            done();
        });
    });

    it('should return competitor 7 for selection 2', function(done) {
        betAg.aggregateSelection(2, function(result) {
            assert.equal(7, result._id);
            done();
        });
    });
});
