'use strict';

require('../');
var MongoClient = require('mongodb').MongoClient;
var assert = require('chai').assert;
var Rabbit = require('node-rabbitmq');
var config = require('ptl-config');
var rabbit;

var redis = require('redis'),
    client = redis.createClient();


var testBets = [{
    Selection1: 1,
    Selection2: 7,
    Selection4: 7,
    Selection5: 7,
    Selection6: 7,
    Selection7: 7,
    Selection8: 7,
    Selection9: 7,
    Selection10: 7

}, {
    Selection1: 1,
    Selection2: 7,
    Selection3: 7
}, {
    Selection1: 1,
    Selection2: 7,
    Selection3: 7
}, {
    Selection1: 2,
    Selection2: 7,
    Selection3: 7
}, {
    Selection1: 3,
    Selection2: 7,
    Selection3: 7
}];

function clearDb(callback) {
    MongoClient.connect(config.mongoUri, function (err, db) {
        db.collection('bets', function (err, col) {

            col.remove({}, function (err) {
                assert.equal(null, err);
                col.insert(testBets,
                    function (err) {
                        assert.equal(null, err);

                        callback();

                    });
            });

        });
    });
}

describe('when notified report will be updated', function () {
    beforeEach(function (done) {

        rabbit = new Rabbit(config.queueUri, function () {
            clearDb(function () {
                client.set('betReport', null);
                done();
            });
        });
    });
    //
    //it('should have the correct count', function (done) {
    //
    //    rabbit.receiveJson('KPI', function (message, that) {
    //
    //        //assert.equals(message.kpiName, "ReportUpdated");
    //        client.get('betReport', function (err, reply) {
    //            that.ack();
    //            that.close();
    //            assert.isNull(err, 'has errors');
    //            assert.isNotNull(reply, 'nothing in the cache');
    //
    //            var report = JSON.parse(reply);
    //            assert.equal(5, report.total);
    //            done();
    //
    //        });
    //    });
    //
    //    rabbit.sendJson({}, 'betSaved');
    //
    //});

});
