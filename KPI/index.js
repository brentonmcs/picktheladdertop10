(function() {
    'use strict';
    var express = require('express');
    var app = express();
    var Rabbit = require('node-rabbitmq');
    var logSender = require('node-log-sender');
    var config = require('ptl-config');

    var rabbit = new Rabbit(config.queueUri, function () {
        var KpiRecorder = require('./lib/kpiRecorder');
        logSender.configure(rabbit);

        var kpiRecorder = new KpiRecorder(rabbit);

        app.get('/', function(req, res) {
            kpiRecorder.getReport(function(report) {
                res.send(report);
            });

            rabbit.sendJson({
                kpiName: 'kpiReportSent'
            }, 'KPI');
        });

        var server = app.listen(3002, function() {
            logSender.info('KPI Server Stated http://' + server.address().address + ':' + server.address().port);
        });
    });

    process.on('uncaughtException', function (err) {
        console.error('uncaughtException:', err.message);
        console.error(err.stack);
        logSender.error(err.message + " \n" + err.stack);
        process.exit(1);
    });

})();
