'use strict';

var rabbit;

var redis = require('redis'),
    client = redis.createClient();

var kpiList = [];

function KpiRecorder(rabbitMq) {
    rabbit = rabbitMq;

    rabbit.receiveJson('KPI', function(message, that) {

        if (kpiList.indexOf(message.kpiName) === -1) {
            kpiList.push(message.kpiName);
            console.log('adding ' + message.kpiName);
        }

        console.log('receiving KPI: ' + JSON.stringify(message));
        client.incr(message.kpiName, function () {});
        that.ack();
    });
}


KpiRecorder.prototype.getReport = function(callback) {

    client.mget(kpiList, function(err, results) {

        var result = [];
        for(var i = 0; i < kpiList.length; i++) {
            result.push({ name: kpiList[i], value: results[i] });
        }
        callback(result);
    });
};

module.exports = KpiRecorder;
